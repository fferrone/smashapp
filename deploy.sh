#!/bin/bash
set -e

mvn clean package -f frontend-aggregator-pom.xml 
mvn clean package -f backend-aggregator-pom.xml
scp -l 8192  backend/target/smashapp-backend.war flavio@baloon.info:/home/flavio
scp -l 8192  frontend/target/smashapp-frontend.war flavio@baloon.info:/home/flavio
