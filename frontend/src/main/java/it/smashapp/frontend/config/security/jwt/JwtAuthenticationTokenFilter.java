package it.smashapp.frontend.config.security.jwt;

import io.jsonwebtoken.*;
import it.smashapp.frontend.config.SecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger log = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String token = request.getHeader("Authorization");
        /**
         * Check token
         */
        if (token != null && token.length() > 7) {
            String authToken = token.substring(7);
            try {
                /**
                 * If token is not valid
                 * the parser will throw an Exception
                 */
                Jws<Claims> claims = Jwts.parser()
                        .setSigningKey(SecurityConfig.JWT_SECRET)
                        .parseClaimsJws(authToken);

                String username = claims.getBody().getSubject();
                /** parse user authorities */
                String[] authList = ((String) claims.getBody().get("auth")).split(",");
                List<SimpleGrantedAuthority> list = new ArrayList<>();
                for (int i = 0; i < authList.length; i++) {
                    SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authList[i]);
                    list.add(grantedAuthority);
                }
                Authentication auth = new UsernamePasswordAuthenticationToken(username, null, list);
                /**
                 * Set SecurityContext authentication
                 */
                SecurityContextHolder.getContext().setAuthentication(auth);


            } catch (ExpiredJwtException e) {
                /**
                 * Return 403 Forbidden on Expired Token
                 */
                log.info("Token Expired: rejected");
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token Expired");

                return;
            } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
                log.info(e.toString());
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token verification failed, malformed token");
                return;
            }
        }

        log.info("JWT token accepted");
        /**
         * Continue the filter chain
         */
        filterChain.doFilter(servletRequest, servletResponse);

    }

    private boolean tokenValid(String token) {
        if (token == null) {
            return false;
        }
        String authToken = token.substring(7);
        Jwt<Header, Claims> claims = Jwts.parser().parseClaimsJwt(authToken);
        String username = claims.getBody().getSubject();
        if (username == null) {
            return false;
        }
        Date expire = claims.getBody().getExpiration();
        if (expire.after(new Date())) {
            return false;
        }
        return true;
    }

    @Override
    public void destroy() {

    }
}
