package it.smashapp.frontend.config.security.jwt;

import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;

public class JwtAuthenticationProvider extends DaoAuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication){
        JwtToken token = (JwtToken) authentication;
        JwtToken authenticated = new JwtToken(token.getUsername(),
                token.getPassword(), token.getAuthorities());
        authenticated.setAuthenticated(true);
        return authenticated;
    }

}
