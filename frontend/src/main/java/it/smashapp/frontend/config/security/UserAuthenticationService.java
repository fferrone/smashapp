package it.smashapp.frontend.config.security;

import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("userAuthenticationService")
public class UserAuthenticationService implements UserDetailsService {
    private final Logger log = LoggerFactory.getLogger(UserAuthenticationService.class);

    @Autowired
    private UserRepository userService;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        log.info("Login attempt with username: " + username);

        User user = userService.findByUsername(username);
        if (user == null) {
            log.info("User " + username + " not found");
            throw new UsernameNotFoundException("Username not found");
        }
        log.info("User logged");
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.isEnabled(), true, true, true, getGrantedAuthorities(user));
    }


    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList();
        user.getRuoli().stream()
                .map(ruolo -> authorities.add(new SimpleGrantedAuthority("ROLE_" + ruolo.getNome())))
                .collect(Collectors.toList());
        return authorities;
    }

}
