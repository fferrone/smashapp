package it.smashapp.frontend.config;

import it.smashapp.common.constant.SecurityConstants;
import it.smashapp.frontend.config.security.LogoutSuccessHandler;
import it.smashapp.frontend.config.security.jwt.JwtAuthenticationTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final Logger log = LoggerFactory.getLogger(SecurityConfig.class);
    @Autowired
    @Qualifier("userAuthenticationService")
    UserDetailsService userDetailsService;

    @Autowired
    LogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    AuthenticationEntryPoint unauthorizedEntryPoint;

    @Autowired
    CorsFilter corsFilter;

    @Autowired
    DataSource dataSource;

    public final static String JWT_SECRET = "smashapp";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /** security rules */
        http
                .authorizeRequests()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/**").hasRole("APP_USER")
                .anyRequest().authenticated()
                .expressionHandler(getExpressionHandler())
                .and()
                .httpBasic()
                .and()
                .formLogin()
        ;
        /** Authentication Provider */
        http.authenticationProvider(authenticationProvider());
        /** Stateless configuration */
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        /** Unauthorized Entry Point */
        http.exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint);
        /** Other Configurations */
        http.csrf().disable();
        /** JWT Filter */
        http.addFilterBefore(authenticationTokenFilterBean(), BasicAuthenticationFilter.class);
        /** CORS Filter */
        http.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
    }

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        JwtAuthenticationTokenFilter authenticationTokenFilter = new JwtAuthenticationTokenFilter();
        authenticationTokenFilter.setAuthenticationManager(authenticationManager());
        return authenticationTokenFilter;

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        /** UserDetailsService */
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
        /** AuthenticationProvider */
        auth.authenticationProvider(authenticationProvider());
        auth
                .inMemoryAuthentication()
                .withUser("user").password("password").roles("APP_USER");
    }

    /**
     * Build our implementation of AuthenticationProvider
     * with a DaoAuthenticationProvider within our custom UserDetailsService
     *
     * @return
     */
    private DaoAuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    /**
     * Define Application User passoword encoder algorithm
     *
     * @return
     */
    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * Configure ExpressionHandler
     * with custom Role Hierarchy
     *
     * @return
     */
    private SecurityExpressionHandler<FilterInvocation> getExpressionHandler() {

        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
        expressionHandler.setRoleHierarchy(roleHierarchy());

        return expressionHandler;
    }

    @Bean
    public RoleHierarchyImpl roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy(SecurityConstants.ROLE_HIERARCHY_STRING_REPRESENTATION);
        return roleHierarchy;
    }
}
