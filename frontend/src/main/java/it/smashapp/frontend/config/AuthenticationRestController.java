package it.smashapp.frontend.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.smashapp.frontend.config.security.jwt.JwtAuthenticationResponse;
import it.smashapp.frontend.config.security.jwt.JwtTokenUtil;
import it.smashapp.frontend.config.security.jwt.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
public class AuthenticationRestController {

    private String tokenHeader = "Authorization";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(HttpServletRequest request) throws AuthenticationException {
        /**
         * Authentication.
         * If auth is not OK an AuthenticationException is thrown
         */
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getParameter("username"),
                        request.getParameter("password")));
        /**
         * Jwt token creation
         */
        String grantedRoles = "";
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            grantedRoles += authority.getAuthority() + ",";
        }
        final String jwtToken = Jwts.builder()
                .setSubject(authentication.getName())
                .claim("auth", grantedRoles)
                .setExpiration(new Date(System.currentTimeMillis() + (hundred_days * 1000)))
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, SecurityConfig.JWT_SECRET).compact();

        /**
         * return jwt token
         */
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken));
    }

    @RequestMapping(path = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    final int seconds_in_a_day = 86400000;
    final long one_day_in_milliseconds = seconds_in_a_day * 1000;
    final int hundred_days = 8640000;//one_day_in_milliseconds * 100;

}
