package it.smashapp.frontend.model.repository;

import it.smashapp.common.model.entity.Immagine;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@PreAuthorize("hasRole('ROLE_APP_USER')")
public interface ImmagineRepository extends PagingAndSortingRepository<Immagine, Integer> {

    List<Immagine> findAll();

    @Query(value = "SELECT i FROM Immagine i WHERE i.stato = 5 ORDER BY i.id DESC")
    List<Immagine> findActiveImages();

    @Query(value = "SELECT i FROM Immagine i, PreferitiCM p WHERE i.id = p.contenutoMultimediale.id AND p.user.id = :user AND i.stato = 5 ORDER BY i.id DESC")
    List<Immagine> findStarredImages(@Param("user") int user);
}
