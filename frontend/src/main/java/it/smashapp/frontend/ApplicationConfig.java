package it.smashapp.frontend;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@ComponentScan("it.smashapp.frontend")
@EntityScan("it.smashapp.common.model.*")
@EnableJpaRepositories("it.smashapp.frontend.model.repository")
public class ApplicationConfig {

}
