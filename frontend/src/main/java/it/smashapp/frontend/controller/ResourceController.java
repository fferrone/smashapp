package it.smashapp.frontend.controller;

import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.repository.UserRepository;
import it.smashapp.frontend.model.repository.ImmagineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.io.IOUtils;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping(path = "/resources")
public class ResourceController {

    @Autowired
    ImmagineRepository immagineRepository;
    @Autowired
    @Qualifier("userRepository")
    UserRepository userRepository;

    @Autowired
    Environment env;


    @ResponseBody
    @RequestMapping(path = "/img/{imgId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] img(@PathVariable(value = "imgId") int imgId,
                      Model model) throws IOException {

        String uploadDir = env.getRequiredProperty("UPLOAD_DIR");
        String imageDir = env.getRequiredProperty("IMAGE_DIR");

        Immagine img = immagineRepository.findOne(imgId);
        String filePath = uploadDir + imageDir + "/" + img.getFileName();
        FileSystemResource resource = new FileSystemResource(new File(filePath));

        return IOUtils.toByteArray(resource.getInputStream());

    }


}
