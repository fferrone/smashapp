package it.smashapp.frontend;

import it.smashapp.common.CommonConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ApplicationInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext dispatcherContext =
                new AnnotationConfigWebApplicationContext();
        /* register Frontend Context */
        dispatcherContext.register(ApplicationConfig.class);
        /* register Common Context */
        dispatcherContext.register(CommonConfig.class);

        ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcher",
                new DispatcherServlet(dispatcherContext));
        dynamic.addMapping("/api/*");
        dynamic.setLoadOnStartup(1);

    }
}
