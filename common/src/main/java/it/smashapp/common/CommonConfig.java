package it.smashapp.common;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "it.smashapp.common")
@EntityScan("it.smashapp.common.model.entity")
@EnableJpaRepositories("it.smashapp.common.repository")
public class CommonConfig {

}
