package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 */

@Entity()
@Table(name = "immagine")
@Inheritance(strategy = InheritanceType.JOINED)
public class Immagine extends ContenutoMultimediale implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2665122680563547155L;

	@Column(name = "file_name")
    private String fileName;

    @Column(name = "path")
    private String path;

    @Column(name = "width")
    private int width;

    @Column(name = "height")
    private int height;

    public Immagine() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String file_name) {
        this.fileName = file_name;
    }

    @Override
    public String toString() {
        return String.format(
                "Immagine[id=%d, name='%s']",
                id, nome);
    }

}
