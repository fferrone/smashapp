package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 */

@Entity()
@Table(name = "video")
@Inheritance(strategy = InheritanceType.JOINED)
public class Video extends ContenutoMultimediale implements Serializable {

    @Column(name = "url")
    private String url;

    @Column(name = "durata")
    private int durata;

    public Video() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    @Override
    public String toString() {
        return String.format(
                "Video[id=%d, name='%s']");
    }

}
