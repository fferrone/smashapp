package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity()
@Table(name = "stato_cm")
public class StatoCM implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Transient
    private int state;

    private String nome;
    private StatoCM(final int state){
        this.state = state;
    }
    protected StatoCM(){}

    public int getState(){
        return this.state;
    }

    @Override
    public String toString(){
        return String.valueOf(this.state);
    }

    public String getNome(){
        return this.nome;
    }


}
