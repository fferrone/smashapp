package it.smashapp.common.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "tipo_evento_cm")
public class TipoEventoCM {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Column
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

}
