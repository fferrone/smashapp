package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "preferiti_user_cm")
public class PreferitiCM implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user")
    protected User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="contenuto_multimediale")
    protected ContenutoMultimediale contenutoMultimediale;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ContenutoMultimediale getContenutoMultimediale() {
        return contenutoMultimediale;
    }

    public void setContenutoMultimediale(ContenutoMultimediale contenutoMultimediale) {
        this.contenutoMultimediale = contenutoMultimediale;
    }

}
