package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "evento_cm")
public class EventoCM implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="tipo_evento")
    protected TipoEventoCM tipoEvento;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user")
    protected User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="contenuto_multimediale")
    protected ContenutoMultimediale contenutoMultimediale;

    @Column(name = "data")
    protected Date data;

    @Column(name = "event_order")
    protected int eventOrder;

    @Column(name = "nota")
    protected String nota;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoEventoCM getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEventoCM tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ContenutoMultimediale getContenutoMultimediale() {
        return contenutoMultimediale;
    }

    public void setContenutoMultimediale(ContenutoMultimediale contenutoMultimediale) {
        this.contenutoMultimediale = contenutoMultimediale;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getEventOrder() {
        return eventOrder;
    }

    public void setEventOrder(int eventOrder) {
        this.eventOrder = eventOrder;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}
