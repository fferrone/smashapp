package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


public class Redattore extends User {

    @ManyToMany(fetch = FetchType.EAGER, targetEntity = RuoloUser.class)
    @JoinTable(name = "canale_redattore",
            joinColumns = {@JoinColumn(name = "redattore")},
            inverseJoinColumns = {@JoinColumn(name = "id")})
    protected Set<Canale> canali = new HashSet();

    public Set<Canale> getCanali() {
        return canali;
    }

    public void setCanali(Set<Canale> canali) {
        this.canali = canali;
    }
}
