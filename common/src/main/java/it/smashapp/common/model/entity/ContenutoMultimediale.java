package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="contenuto_multimediale")
@Inheritance(strategy= InheritanceType.JOINED)
public abstract class ContenutoMultimediale implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2008882813604322629L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;
    @Column(name = "nome", nullable = false)
    protected String nome;
    @Column(name = "titolo")
    protected String titolo;
    @Column(name = "descrizione")
    protected String descrizione;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="stato", nullable = false)
    protected StatoCM stato;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="canale")
    protected Canale canale;

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public StatoCM getStato() {
        return stato;
    }

    public void setStato(StatoCM stato) {
        this.stato = stato;
    }

    public Canale getCanale() {
        return canale;
    }

    public void setCanale(Canale canale) {
        this.canale = canale;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return getNome();
    }

}
