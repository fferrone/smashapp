package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity()
@Table(name = "canale_redattore")
public class CanaleRedattore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @JoinColumn(name="canale")
    protected Canale canale;

    @JoinColumn(name="redattore")
    protected User redattore;

    public CanaleRedattore() {
    }

    public int getId() {
        return id;
    }

}
