package it.smashapp.common.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "ruolo_user")
public class RuoloUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;
    @Column(name = "nome")
    private String nome;

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return getNome();
    }
}
