package it.smashapp.common.repository;

import it.smashapp.common.model.entity.RuoloUser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("roleRepository")
public interface RoleRepository extends CrudRepository<RuoloUser, Integer> {

    List<RuoloUser> findAll();
    RuoloUser findByNome(String name);
}
