package it.smashapp.common.repository;

import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.PreferitiCM;
import it.smashapp.common.model.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Qualifier("preferitiCMRepository")
public interface PreferitiCMRepository extends CrudRepository<PreferitiCM, Integer> {

    List<PreferitiCM> findAll();

    PreferitiCM findByUser(@Param("user")User user);

    PreferitiCM findByContenutoMultimediale(@Param("contenutoMultimediale")ContenutoMultimediale contenutoMultimediale);

    PreferitiCM findByUserAndContenutoMultimediale(
            @Param("user")User user,
            @Param("contenutoMultimediale")ContenutoMultimediale cm);
}
