package it.smashapp.common.repository;

import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.EventoCM;
import it.smashapp.common.model.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("eventoRepository")
public interface EventoCMRepository extends CrudRepository<EventoCM, Integer> {

    List<EventoCM> findAll();
    List<EventoCM> findByUser(User user);
    List<EventoCM> findByContenutoMultimedialeOrderByEventOrderDesc(ContenutoMultimediale contenutoMultimediale);
    @Query(value = "select max(e.eventOrder) from EventoCM e " +
            "join ContenutoMultimediale cm on e.contenutoMultimediale.id = cm.id" +
            " where cm.id = ?1")
    Integer findMaxEventOrderByCM(int cm);
}
