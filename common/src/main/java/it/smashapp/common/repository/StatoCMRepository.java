package it.smashapp.common.repository;

import it.smashapp.common.model.entity.StatoCM;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("roleRepository")
public interface StatoCMRepository extends CrudRepository<StatoCM, Integer> {

    List<StatoCM> findAll();
    StatoCM findByNome(String name);
}
