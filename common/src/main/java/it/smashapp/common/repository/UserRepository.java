package it.smashapp.common.repository;

import it.smashapp.common.model.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("userRepository")
public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findAll();

    User findByUsername(@Param("username") String username);
}
