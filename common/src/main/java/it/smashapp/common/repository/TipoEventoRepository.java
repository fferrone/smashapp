package it.smashapp.common.repository;

import it.smashapp.common.model.entity.TipoEventoCM;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("tipoEventoRepository")
public interface TipoEventoRepository extends CrudRepository<TipoEventoCM, Integer> {

    List<TipoEventoCM> findAll();

    TipoEventoCM findByNome(String nome);
}
