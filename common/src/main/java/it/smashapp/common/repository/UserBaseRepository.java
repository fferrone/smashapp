package it.smashapp.common.repository;

import it.smashapp.common.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface UserBaseRepository<T extends User>  extends CrudRepository<T, Long> {

//    @Query(value = "select u.* from user as u, ruolo_user r, user_ruolo_user uru where uru.ruolo_user = r.id AND \n" +
//            "  uru.user = u.id and r.nome = "+ User.class, nativeQuery = true)
    List<T> findAll();
}
