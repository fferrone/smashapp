package it.smashapp.common.repository;

import it.smashapp.common.model.entity.StatoUser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("roleRepository")
public interface StatoUserRepository extends CrudRepository<StatoUser, Integer> {

    List<StatoUser> findAll();
    StatoUser findByNome(String name);
}
