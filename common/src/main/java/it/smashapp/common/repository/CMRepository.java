package it.smashapp.common.repository;

import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.StatoCM;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CMRepository <T extends ContenutoMultimediale> extends CrudRepository<T, Integer> {

    List<T> findAll();
    List<T> findByStato(StatoCM stato);
}
