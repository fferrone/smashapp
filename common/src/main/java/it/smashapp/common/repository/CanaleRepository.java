package it.smashapp.common.repository;

import it.smashapp.common.model.entity.Canale;
import it.smashapp.common.model.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("canaleRepository")
public interface CanaleRepository extends CrudRepository<Canale, Integer> {
    List<Canale> findAll();

    Canale findByNome(String name);

    /* ho provato a farla con una sola join ma cr.redattore.id = ?1
     * non risolve correttamente l'attributo
     * da confrontare e confronta cr.id
     */
    @Query(value = "select c from Canale c join CanaleRedattore cr " +
            "on c.id = cr.canale " +
            "join User u on cr.redattore = u.id " +
            "where u.id = ?1")
    List<Canale> findByOwner(int id);
}
