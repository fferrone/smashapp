package it.smashapp.common.constant;

public interface Constants {

    interface RUOLO_USER {
        String ADMIN = "ADMIN";
        String APP_USER = "APP_USER";
        String REDATTORE = "REDATTORE";
        String APPROVATORE = "APPROVATORE";
        String GUEST = "GUEST";
    }

    interface STATO_USER {
        String ACTIVE = "ACTIVE";
        String INACTIVE = "INACTIVE";
        String DELETED = "DELETED";
        String LOCKED = "LOCKED";
    }
    interface STATO_CM {
        String IN_PROPOSAL = "IN PROPOSAL";
        String INACTIVE = "INACTIVE";
        String DELETED = "DELETED";
        String LOCKED = "LOCKED";
        String APPROVED = "APPROVED";
        String REJECTED = "REJECTED";
    }

    interface TIPO_EVENTO_CM {
        String INSERT = "INSERT";
        String DELETE = "DELETE";
        String APPROVE = "APPROVE";
        String REJECT = "REJECT";
        String SIGNAL = "SIGNAL";
    }

}
