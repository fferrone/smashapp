package it.smashapp.common.constant;

public class SecurityConstants {

    public static final String ROLE_HIERARCHY_STRING_REPRESENTATION =
            "ROLE_" + Constants.RUOLO_USER.ADMIN + " > ROLE_" + Constants.RUOLO_USER.REDATTORE +
                    " " +
                    "ROLE_" + Constants.RUOLO_USER.ADMIN + " > ROLE_" + Constants.RUOLO_USER.APPROVATORE +
                    " " +
                    "ROLE_" + Constants.RUOLO_USER.APPROVATORE + " > ROLE_" + Constants.RUOLO_USER.APP_USER +
                    " " +
                    "ROLE_" + Constants.RUOLO_USER.REDATTORE + " > ROLE_" + Constants.RUOLO_USER.APP_USER +
                    " " +
                    "ROLE_" + Constants.RUOLO_USER.APP_USER + " > ROLE_" + Constants.RUOLO_USER.GUEST;
}
