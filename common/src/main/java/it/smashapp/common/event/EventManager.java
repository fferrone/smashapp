package it.smashapp.common.event;

import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.EventoCM;
import it.smashapp.common.model.entity.TipoEventoCM;
import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.EventoCMRepository;
import it.smashapp.common.repository.TipoEventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class EventManager {

    @Autowired
    EventoCMRepository eventoCMRepository;
    @Autowired
    TipoEventoRepository tipoEventoRepository;

    public void registerEvent(String tipoEvento, User user,
                              ContenutoMultimediale cm,
                              String nota) {

        TipoEventoCM tipoEventoCM = tipoEventoRepository.findByNome(tipoEvento);
        Integer order = eventoCMRepository.findMaxEventOrderByCM(cm.getId());
        order = (order == null ? 1 : order+1);

        EventoCM evento = new EventoCM();
        evento.setUser(user);
        evento.setContenutoMultimediale(cm);
        evento.setData(new Date());
        evento.setTipoEvento(tipoEventoCM);
        evento.setNota(nota);
        evento.setEventOrder(order);

        eventoCMRepository.save(evento);

    }
}
