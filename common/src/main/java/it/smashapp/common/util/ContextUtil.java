package it.smashapp.common.util;

import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ContextUtil {

    @Autowired
    UserRepository userRepository;

    public User getCurrentUser(){

        return userRepository.
                findByUsername(SecurityContextHolder.getContext()
                        .getAuthentication().getName());
    }

}
