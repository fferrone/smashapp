package it.smashapp.backend.config.security;

import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service("userAuthenticationService")
public class UserAuthenticationService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        System.out.println("User : " + user);
        if (user == null) {
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.getStato().getNome().equals("ACTIVE"), true, true, true, getGrantedAuthorities(user));
    }

    /**
     * Provides the User Granted Authorities
     **/
    private List<GrantedAuthority> getGrantedAuthorities(User user) {

        List<GrantedAuthority> authorities =
                user.getRuoli().stream()
                        .map(ruolo -> new SimpleGrantedAuthority("ROLE_" + ruolo.getNome()))
                        .collect(Collectors.toList());

        return authorities;
    }

}
