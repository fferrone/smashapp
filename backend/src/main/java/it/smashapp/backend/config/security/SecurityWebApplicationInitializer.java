package it.smashapp.backend.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring Security Filter Chain
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {}