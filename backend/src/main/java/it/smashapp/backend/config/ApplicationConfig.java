package it.smashapp.backend.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.servlet.ServletException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "it.smashapp.backend")
@EnableJpaRepositories("it.smashapp.backend.model.repository")
public class ApplicationConfig {

    private final Logger log = LoggerFactory.getLogger(ApplicationConfig.class);

    @Autowired
    Environment env;

    @Bean
    CommandLineRunner init() throws ServletException {
        String uploadDir = env.getRequiredProperty("UPLOAD_DIR");
        String imageDir = env.getRequiredProperty("IMAGE_DIR");
        if (!Files.exists(Paths.get(uploadDir))) {
            try {
                Files.createDirectories(Paths.get(uploadDir));
            } catch (IOException e) {
                log.error(e.toString());
                throw new ServletException("Impossibile creare Upload Directory", e);
            }

        }
        if (!Files.exists(Paths.get(uploadDir+imageDir))) {
            try {
                Files.createDirectories(Paths.get(uploadDir+imageDir));
            } catch (IOException e) {
                log.error(e.toString());
                throw new ServletException("Impossibile creare image directory", e);
            }

        }
        return (String[] args) -> {
            log.info("Upload Directory already present");
        };
    }
}
