package it.smashapp.backend.config.security;

import it.smashapp.common.constant.Constants;
import it.smashapp.common.constant.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userAuthenticationService")
    UserDetailsService userDetailsService;

    @Autowired
    @Qualifier("authenticationSuccessHandler")
    AuthenticationSuccessHandler authenticationSuccessHandler;

    /**
     * Configure Application Http Routes Security
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /* le regole piu restrittive vanno messe prima */
        http
                .authorizeRequests()
                .antMatchers("/", "/login").permitAll()
                .antMatchers("/workspace/admin/**").hasRole(Constants.RUOLO_USER.ADMIN)
                .antMatchers("/workspace/redattore/**").hasRole(Constants.RUOLO_USER.REDATTORE)
                .antMatchers("/workspace/**").hasAnyRole(Constants.RUOLO_USER.REDATTORE, Constants.RUOLO_USER.APPROVATORE, Constants.RUOLO_USER.ADMIN)
                .anyRequest().authenticated()
                .expressionHandler(getExpressionHandler())
                .and()
                .formLogin().loginPage("/login")
                .usernameParameter("username").passwordParameter("password")
                .and()
                .logout().deleteCookies().logoutUrl("/logout").logoutSuccessUrl("/login?logout")
                .permitAll()
                .and().csrf()
                .and().exceptionHandling().accessDeniedPage("/login");
    }

    /**
     * Init AuthenticationManager through the AuthenticationManagerBuilder
     * configuring the ProviderManager with our custom AuthenticationProvider
     * and the UserDetailsService
     *
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        /** UserDetailsService */
        auth.userDetailsService(userDetailsService);
        /** AuthenticationProvider */
        auth.authenticationProvider(authProvider());

//        auth.inMemoryAuthentication()
//                .withUser("admin").password("admin").roles(Constants.RUOLO_USER.ADMIN)
//                .and()
//                .withUser("user").password("password").roles(Constants.RUOLO_USER.APP_USER)
//                .and()
//                .withUser("approvatore").password("password").roles(Constants.RUOLO_USER.APP_USER, Constants.RUOLO_USER.APPROVATORE);
    }

    /**
     * Build our implementation of AuthenticationProvider
     * with a DaoAuthenticationProvider within our custom UserDetailsService
     *
     * @return
     */
    @Bean
    public DaoAuthenticationProvider authProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    /**
     * Define Application User passoword encoder algorithm
     *
     * @return
     */
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    /**
     * Configure ExpressionHandler
     * with custom Role Hierarchy
     *
     * @return
     */
    private SecurityExpressionHandler<FilterInvocation> getExpressionHandler() {

        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
        expressionHandler.setRoleHierarchy(roleHierarchy());

        return expressionHandler;
    }

    @Bean
    public RoleHierarchyImpl roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy(SecurityConstants.ROLE_HIERARCHY_STRING_REPRESENTATION);
        return roleHierarchy;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver(){

        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        return resolver;
    }
}