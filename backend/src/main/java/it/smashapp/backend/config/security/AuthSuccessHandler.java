package it.smashapp.backend.config.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component("authenticationSuccessHandler")
public class AuthSuccessHandler implements org.springframework.security.web.authentication.AuthenticationSuccessHandler{
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        User principal = (User)authentication.getPrincipal();

        SpringSecurityDialect dialect = new SpringSecurityDialect();
        boolean redattore = httpServletRequest.isUserInRole("ROLE_ADMIN");

        boolean role_admin = authentication.getAuthorities().contains("ROLE_ADMIN");
        if(redattore){
            System.out.println("Authorities: " + ((User)principal).getAuthorities());
        }
        httpServletResponse.sendRedirect("/workspace");

    }
}
