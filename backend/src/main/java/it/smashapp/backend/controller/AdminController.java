package it.smashapp.backend.controller;

import it.smashapp.backend.model.repository.ContenutoMultimedialeRepository;
import it.smashapp.backend.service.CMService;
import it.smashapp.backend.service.StatoUserService;
import it.smashapp.backend.util.MessageHelper;
import it.smashapp.common.constant.Constants;
import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.RuoloUser;
import it.smashapp.common.model.entity.StatoUser;
import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.RoleRepository;
import it.smashapp.common.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/workspace/admin")
public class AdminController {

    private final Logger log = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    StatoUserService statoUserService;
    @Autowired
    ContenutoMultimedialeRepository cmRepository;
    @Autowired
    CMService cmService;

    @Autowired
    PasswordEncoder encoder;

    @RequestMapping(path = "/create-user", method = RequestMethod.GET)
    public String createUser(Model model, HttpServletRequest request) {
        return "admin/create-user";
    }

    @RequestMapping(path = "/create-user", method = RequestMethod.POST)
    public String submitCreateUser(@RequestParam("username") String username,
                                   @RequestParam("password") String password,
                                   @RequestParam("passwordCheck") String passwordCheck,
                                   @RequestParam("role") String role,
                                   RedirectAttributes attributes, HttpServletRequest request) throws IOException {

        if (!password.equals(passwordCheck)) {
           MessageHelper.errorMessage(attributes, "Le password non coincidono.");
            return "redirect:/workspace/admin/create-user";
        }
        User user = new User();
        user.setUsername(username);
        user.setPassword(encoder.encode(password));
        StatoUser stato = statoUserService.get(Constants.STATO_USER.ACTIVE);
        if (stato == null) {
            log.error("Impossibile recuperare Stato User: " + Constants.STATO_USER.ACTIVE);
            MessageHelper.errorMessage(attributes, "Impossibile completare l'operazione.");
            return "redirect:/workspace/admin/create-user";
        }
        user.setStato(stato);
        user.setEnabled(true);

        RuoloUser ruolo = roleRepository.findByNome(role);
        if (ruolo == null) {
            log.error("Impossibile recuperare Ruolo User id: " + role);
            MessageHelper.errorMessage(attributes, "Impossibile completare l'operazione. Ruolo non definito.");
            return "redirect:/workspace/admin/create-user";
        }
        user.getRuoli().add(ruolo);
        userRepository.save(user);

        MessageHelper.successMessage(attributes, "Nuovo utente creato.");
        return "redirect:/workspace/admin/user-list";
    }

    @RequestMapping(path = "/delete-user", method = RequestMethod.POST)
    public String deleteUser(@RequestParam("userId") int userId,
                             RedirectAttributes attributes, HttpServletRequest request) {

        User user = userRepository.findOne(userId);

        if (user == null) {
            attributes.addAttribute("error", "Impossibile recuperare Utente.");
            return "redirect:/workspace/admin/user-list";
        }
        try {
            userRepository.delete(userId);
        } catch (DataIntegrityViolationException e) {
            MessageHelper.errorMessage(attributes, e.getMessage());
            return "redirect:/workspace/admin/user-list";
        }
        MessageHelper.successMessage(attributes, "Utente eliminato.");
        return "redirect:/workspace/admin/user-list";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/delete-cm")
    public String deleteCM(@RequestParam("cmId") int cmId,
                            @RequestParam(name = "nota", defaultValue = "") String nota,
                            RedirectAttributes attributes) {

        ContenutoMultimediale cm = cmRepository.findOne(cmId);

        cmService.changeCMState(cm, nota, Constants.STATO_CM.DELETED, Constants.TIPO_EVENTO_CM.DELETE);

        MessageHelper.successMessage(attributes, "Contenuto Multimediale ELIMINATO");
        return "redirect:/workspace/dettaglio-cm-img/" + cmId;
    }
    @RequestMapping(path = "/user-list", method = RequestMethod.GET)
    public String userList(Model model) {
        List<User> userList = userRepository.findAll();
        model.addAttribute("userList", userList);
        return "admin/user-list";
    }
}
