package it.smashapp.backend.controller;

import it.smashapp.backend.model.repository.ContenutoMultimedialeRepository;
import it.smashapp.backend.model.repository.ImageRepository;
import it.smashapp.backend.model.repository.VideoRepository;
import it.smashapp.common.model.entity.EventoCM;
import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.model.entity.Video;
import it.smashapp.common.repository.EventoCMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/workspace")
public class WorkspaceController {

    private final Logger log = LoggerFactory.getLogger(WorkspaceController.class);

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    VideoRepository videoRepository;
    @Autowired
    EventoCMRepository eventRepository;
    @Autowired
    ContenutoMultimedialeRepository cmRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String workspace(Model model) {

        return "workspace";
    }

    @RequestMapping(path = "/elenco-cm", method = RequestMethod.GET)
    public String elencoCM(Model model) {

        List<Immagine> images = imageRepository.findAll();
        List<Video> videos = videoRepository.findAll();

        model.addAttribute("images", images);
        model.addAttribute("videos", videos);

        return "elenco-cm";
    }

    @RequestMapping(path = "/dettaglio-cm-img/{cmId}", method = RequestMethod.GET)
    public String dettaglioCMImg(@PathVariable(value="cmId") int cmId,
                              Model model) {

        Immagine image = imageRepository.findOne(cmId);
        List<EventoCM> eventList =
                eventRepository.findByContenutoMultimedialeOrderByEventOrderDesc(image);

        model.addAttribute("image", image);
        model.addAttribute("eventList", eventList);

        return "dettaglio-cm-img";
    }
}
