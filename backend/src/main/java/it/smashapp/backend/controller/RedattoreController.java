package it.smashapp.backend.controller;

import it.smashapp.backend.model.repository.ImageRepository;
import it.smashapp.common.constant.Constants;
import it.smashapp.common.event.EventManager;
import it.smashapp.common.model.entity.Canale;
import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.CanaleRepository;
import it.smashapp.common.repository.StatoCMRepository;
import it.smashapp.common.repository.UserRepository;
import it.smashapp.common.util.ContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@Controller
@RequestMapping(path = "/workspace/redattore")
public class RedattoreController {

    private final Logger log = LoggerFactory.getLogger(RedattoreController.class);

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CanaleRepository canaleRepository;
    @Autowired
    StatoCMRepository statoCMRepository;
    @Autowired
    EventManager eventManager;
    @Autowired
    ContextUtil contextUtil;
    @Autowired
    Environment env;

    @RequestMapping(method = RequestMethod.POST, path = "/submit-image")
    public String handleImageSubmit(@RequestParam("name") String name,
                                    @RequestParam("titolo") String titolo,
                                    @RequestParam("descrizione") String descrizione,
                                    @RequestParam("canale") int canale,
                                    @RequestParam("nota") String nota,
                                    @RequestParam("file") MultipartFile multipartFile,
                                    RedirectAttributes attributes) {

        String path = env.getProperty("UPLOAD_DIR");
        String imagePath = env.getProperty("IMAGE_DIR");

        if (name.contains("/")) {
            attributes.addFlashAttribute("error", "Non è possibile inserire separatori nel nome");
            return "redirect:/workspace/redattore/create-cm";
        }
        if (multipartFile.isEmpty()) {
            attributes.addFlashAttribute("error",
                    "Il contenuto del file " + name + " è vuoto");
            return "redirect:/workspace/redattore/create-cm";
        }

        try {
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File(path + imagePath + "/" +
                                    multipartFile.getOriginalFilename())));
            FileCopyUtils.copy(multipartFile.getInputStream(), stream);
            stream.close();
        } catch (Exception e) {
            log.error("Errore nella creazione del CM: " + e.getMessage());
            attributes.addFlashAttribute("error",
                    "Errore nella creazione del CM: " + name + " => " + e.getMessage());
            return "redirect:/workspace/redattore/create-cm";
        }

        Immagine immagine = new Immagine();
        immagine.setNome(name);
        immagine.setTitolo(titolo);
        immagine.setDescrizione(descrizione);
        immagine.setPath(imagePath);
        immagine.setFileName(multipartFile.getOriginalFilename());
        immagine.setStato(statoCMRepository.findByNome(Constants.STATO_CM.IN_PROPOSAL));
        Canale channel = canaleRepository.findOne(canale);
        immagine.setCanale(channel);
        /* persist */
        imageRepository.save(immagine);
        /* register event */
        eventManager.registerEvent(Constants.TIPO_EVENTO_CM.INSERT, contextUtil.getCurrentUser(), immagine, nota);

        attributes.addFlashAttribute("message",
                "Nuovo CM creato: " + name + "");

        return "redirect:/workspace";
    }

    @RequestMapping(path = "/create-cm", method = RequestMethod.GET)
    public String createUser(Model model, HttpServletRequest request) {
        List<Canale> channels = canaleRepository.findByOwner(contextUtil.getCurrentUser().getId());
        model.addAttribute("channels" , channels);
        return "redattore/create-cm";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/submit-channel")
    public String handleCreateChannel(@RequestParam("name") String name,
                                      @RequestParam("titolo") String titolo,
                                      @RequestParam("descrizione") String descrizione,
                                      RedirectAttributes attributes) {

        Canale canale = new Canale();
        canale.setNome(name);
        canale.setTitolo(titolo);
        canale.setDescrizione(descrizione);
        User redattore = contextUtil.getCurrentUser();
        canale.getOwners().add(redattore);

        canaleRepository.save(canale);

        attributes.addFlashAttribute("message",
                "Nuovo Canale creato: " + name + "");
        return "redirect:/workspace";
    }


}
