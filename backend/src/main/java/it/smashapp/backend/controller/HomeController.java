package it.smashapp.backend.controller;

import it.smashapp.backend.model.repository.ImageRepository;
import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.model.entity.User;
import it.smashapp.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(path = "/home")
public class HomeController {

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    @Qualifier("userRepository")
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String home(Model model) {
        List<Immagine> list = imageRepository.findAll();
        List<User> userList = userRepository.findAll();
        model.addAttribute("userList", userList);
        model.addAttribute("list", list);
        model.addAttribute("smashapp", "STOCAZZO");
        return "home";
    }


}
