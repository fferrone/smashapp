package it.smashapp.backend.controller;

import it.smashapp.backend.model.repository.ContenutoMultimedialeRepository;
import it.smashapp.backend.model.repository.ImageRepository;
import it.smashapp.backend.model.repository.VideoRepository;
import it.smashapp.backend.service.CMService;
import it.smashapp.backend.util.MessageHelper;
import it.smashapp.common.constant.Constants;
import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.model.entity.StatoCM;
import it.smashapp.common.model.entity.Video;
import it.smashapp.common.repository.EventoCMRepository;
import it.smashapp.common.repository.StatoCMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping(path = "/workspace/approvatore")
public class ApprovatoreController {

    private final Logger log = LoggerFactory.getLogger(ApprovatoreController.class);

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    VideoRepository videoRepository;
    @Autowired
    EventoCMRepository eventRepository;
    @Autowired
    ContenutoMultimedialeRepository cmRepository;
    @Autowired
    StatoCMRepository statoCMRepository;
    @Autowired
    CMService cmService;

    @RequestMapping(path = "/cm-in-proposta", method = RequestMethod.GET)
    public String elencoCM(Model model) {

        StatoCM statoInProposal = statoCMRepository.findByNome(Constants.STATO_CM.IN_PROPOSAL);

        List<Immagine> images = imageRepository.findByStato(statoInProposal);

        List<Video> videos = videoRepository.findByStato(statoInProposal);

        model.addAttribute("images", images);
        model.addAttribute("videos", videos);

        return "approvatore/list-cm-in-proposal";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/approve-cm")
    public String approveCM(@RequestParam("cmId") int cmId,
                            @RequestParam(name = "nota", defaultValue = "") String nota,
                            RedirectAttributes attributes) {

        ContenutoMultimediale cm = cmRepository.findOne(cmId);

        cmService.changeCMState(cm, nota, Constants.STATO_CM.APPROVED, Constants.TIPO_EVENTO_CM.APPROVE);

        MessageHelper.successMessage(attributes, "Contenuto Multimediale APPROVATO");
        return "redirect:/workspace/dettaglio-cm-img/" + cmId;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/reject-cm")
    public String rejectCM(@RequestParam("cmId") int cmId,
                            @RequestParam(name = "nota", defaultValue = "") String nota,
                            RedirectAttributes attributes) {

        ContenutoMultimediale cm = cmRepository.findOne(cmId);

        cmService.changeCMState(cm, nota, Constants.STATO_CM.REJECTED, Constants.TIPO_EVENTO_CM.REJECT);

        MessageHelper.successMessage(attributes, "Contenuto Multimediale RESPINTO");
        return "redirect:/workspace/dettaglio-cm-img/" + cmId;
    }

}
