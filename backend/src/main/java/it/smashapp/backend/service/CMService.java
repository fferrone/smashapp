package it.smashapp.backend.service;

import it.smashapp.backend.model.repository.ContenutoMultimedialeRepository;
import it.smashapp.common.event.EventManager;
import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.repository.StatoCMRepository;
import it.smashapp.common.util.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CMService {

    @Autowired
    ContenutoMultimedialeRepository cmRepository;
    @Autowired
    EventManager eventManager;
    @Autowired
    ContextUtil contextUtil;
    @Autowired
    StatoCMRepository statoCMRepository;


    public String changeCMState(ContenutoMultimediale cm,
                            String nota, String stato, String evento) {

        cm.setStato(statoCMRepository.findByNome(stato));
        cmRepository.save(cm);

        eventManager.registerEvent(evento, contextUtil.getCurrentUser(), cm, nota);

        return "redirect:/workspace/dettaglio-cm-img/" + cm.getId();
    }

}
