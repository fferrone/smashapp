package it.smashapp.backend.service;

import it.smashapp.common.model.entity.StatoUser;
import it.smashapp.common.repository.StatoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatoUserService {

    @Autowired
    StatoUserRepository statoUserRepository;

    public StatoUser get(String stato){
        return statoUserRepository.findByNome(stato);
    }
}
