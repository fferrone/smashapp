package it.smashapp.backend.util;

import org.springframework.ui.Model;

public class MessageHelper {

    public static final String MESSAGE = "message";
    public static final String ERROR = "error";

    public static void successMessage(Model model, String message){
        model.addAttribute(MESSAGE, message);
    }

    public static void errorMessage(Model model, String message){
        model.addAttribute(ERROR, message);
    }
}
