package it.smashapp.backend.model.repository;

import it.smashapp.common.model.entity.ContenutoMultimediale;
import it.smashapp.common.repository.CMRepository;

public interface ContenutoMultimedialeRepository extends CMRepository<ContenutoMultimediale> {

}
