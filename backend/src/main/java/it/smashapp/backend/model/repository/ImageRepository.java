package it.smashapp.backend.model.repository;

import it.smashapp.common.model.entity.Immagine;
import it.smashapp.common.repository.CMRepository;

import java.util.List;

public interface ImageRepository extends CMRepository<Immagine> {

    @Override
    List<Immagine> findAll();
}
