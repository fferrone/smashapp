package it.smashapp.backend.model.repository;

import it.smashapp.common.model.entity.Video;
import it.smashapp.common.repository.CMRepository;

public interface VideoRepository extends CMRepository<Video> {

}
