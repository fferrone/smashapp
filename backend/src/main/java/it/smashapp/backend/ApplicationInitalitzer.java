package it.smashapp.backend;

import it.smashapp.backend.config.ApplicationConfig;
import it.smashapp.common.CommonConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ApplicationInitalitzer implements WebApplicationInitializer {

    public static String ROOT = "upload-dir";
    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext dispatcherContext =
                new AnnotationConfigWebApplicationContext();

        dispatcherContext.register(ApplicationConfig.class);
        dispatcherContext.register(CommonConfig.class);
        servletContext.addListener(new ContextLoaderListener(dispatcherContext));
        ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcher",
                new DispatcherServlet(dispatcherContext));
        dynamic.addMapping("/");
        dynamic.setLoadOnStartup(1);
    }

}
