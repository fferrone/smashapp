var services = angular.module('AppService', ['ngResource']);

services
/** the Login Service */
    .factory('loginService', ['$resource', 'globalConfig', function ($resource, globalConfig) {

        var baseUrl = globalConfig.app_config.base_url;
        return $resource(baseUrl + '/api/login', {},
            {
                authenticate: {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    }
                },
            }
        );
    }])

;