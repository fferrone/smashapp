angular.module('smashapp')
    .controller('MainCtrl', ['$scope', '$http', 'globalConfig', function ($scope, $http, globalConfig) {

        var baseUrl = globalConfig.app_config.base_url;

            $http.get(baseUrl + '/api/immagines/search/findActiveImages').success(function (data) {

                var images = data._embedded.immagines;

                for(image of images){
                    console.log(image);
                }
                $scope.baseUrl = baseUrl;
                $scope.images = images;

            }).error(function(error){
            });
    }]);
