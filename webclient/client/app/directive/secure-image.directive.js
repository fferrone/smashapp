'use strict';

angular.module('smashapp')
    .directive('secureImage', ['globalConfig', function(globalConfig) {
        return {
            restrict: 'E',
            scope: {},
            template: '<img class="img-responsive" http-src="{{url}}" />',
            link: function(scope, element, attrs) {
                scope.url = globalConfig.app_config.base_url + attrs['imgUrl'];
            }
        }
    }]);