angular.module('smashapp')
    .config(['$stateProvider', function ($stateProvider) {
        'use strict';
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/login/login.html',
                controller: 'LoginCtrl'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'app/about/about.html'
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutCtrl',
            })
        ;
    }]);
