angular.module('smashapp', [
    'ui.bootstrap',
    'ui.router',
    'ngCookies',
    'angular.img',
    'AppService'
])
    .config(['$urlRouterProvider', '$locationProvider', '$httpProvider',
        function($urlRouterProvider, $locationProvider, $httpProvider) {
            'use strict';

            $locationProvider.html5Mode(true);
            $urlRouterProvider.otherwise('/');

            /**
             Check for response error.
             Redirect to /login on unauthenticated requests
             */
            $httpProvider.interceptors.push(function($q, $rootScope, $location) {
                    return {
                        'responseError': function(rejection) {
                            var status = rejection.status;
                            var config = rejection.config;
                            var method = config.method;
                            var url = config.url;
                            if(status == 401 || status == 403) {
                                localStorage.removeItem("jwtToken");
                                $rootScope.authenticated = false;
                                $location.path("/login");
                            } else {
                                $rootScope.error = method + " on " + url + " failed with status " + status;
                            }
                            return $q.reject(rejection);
                        }
                    };
                }
            );

            /* Registers auth token interceptor, auth token is either passed by header or by query parameter
             * as soon as there is an authenticated user */
            $httpProvider.interceptors.push(function($q, $rootScope) {
                    return {
                        'request': function(config) {
                            var token = localStorage.getItem("jwtToken");
                            if(token) {
                                $rootScope.authenticated = true;
                                config.headers['Authorization'] = token;
                            }
                            return config || $q.when(config);
                        }
                    };
                }
            );


        }])
    .provider('globalConfig', function() {
        var options = {};
        this.config = function(opt) {
            angular.extend(options, opt);
        };
        this.$get = ['$http', function() {
            if(!options) {
                throw new Error('Impossibile inizializzare l\'applicazione.');
            }
            return options;
        }];
    })
    .run(['$rootScope', '$state', function($rootScope, $state) {
        'use strict';

        // this is available from all across the app
        $rootScope.appName = 'smashapp';

        // make $state available from templates
        $rootScope.$state = $state;
    }])
    .controller("LogoutCtrl", ["$location", "$rootScope", function($location, $rootScope) {
        localStorage.removeItem("jwtToken");
        $location.path("/login");
        $rootScope.authenticated = false;
    }
    ]);
