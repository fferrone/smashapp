angular.module('smashapp')
    .controller('LoginCtrl', ['$scope', '$http', 'loginService', '$rootScope', '$location', function ($scope, $http, loginService, $rootScope, $location) {
        $scope.login = function () {
            loginService.authenticate($.param({
                username: $scope.username, password: $scope.password
            }), function (response) {
                $scope.loginError = null;
                /* save token in the correct form */
                var jwtToken = 'Bearer ' + response.token;
                /* save token in the local storage */
                localStorage.setItem("jwtToken", jwtToken);
                $rootScope.authenticated = true;
                $location.path("/");
            }, function(error){
                console.log("ERROR: " + error.status);
                $scope.loginError = "Credenziali errate.";
            });
        };
    }]);
