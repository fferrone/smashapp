package it.smashapp.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

import it.smashapp.android.layout.OfflineModeListAdapter;
import it.smashapp.android.model.OfflineModeDatabaseManager;

/**
 * Created by alessio on 12/15/16.
 */

public class OfflineModeFile {

    private static final String LOG_TAG = "OfflineModeFile";
    private File file;
    private String fileName;
//    private OfflineModeDatabaseManager omDBM;

    public OfflineModeFile(Context context, String nameCM) {

        /* edit image name to store it */
        fileName = nameCM.replaceAll(" ", "_").toLowerCase() + ".jpg";

        /* prepare smashapp filesystem */
        String dirPath = context.getFilesDir().getAbsolutePath() + File.separator + "offlinemode/";
        File appDir = new File(dirPath);
        file = new File(appDir + File.separator + fileName);

        /* check if app directory exist, otherwise it creates */
        if (!appDir.exists()) {
            if (appDir.mkdir()) {
                Log.i(LOG_TAG, ("===> App directory created!"));
                Log.i(LOG_TAG, ("===> " + appDir));
            } else {
                Log.i(LOG_TAG, ("===> App directory not created."));
            }
        } else {
            Log.i(LOG_TAG, ("===> SmashApp file system checked!"));
            Log.i(LOG_TAG, ("===> " + appDir));
        }
    }

    public String getFilePath(){
        return file.getAbsolutePath();
    }

    public boolean isExist() {
        return file.exists();
    }

    public Bitmap getCM() {

        if (isExist()) {
            Bitmap cm = BitmapFactory.decodeFile(file.getAbsolutePath());
            Log.i(LOG_TAG, ("===> File took!"));
            return cm;
        }

        return null;
    }

    public String save(Bitmap bm) {

        if (!isExist()) {
            try {
                FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath().toString()), true);
                bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
                Log.i(LOG_TAG, ("===> Row saved!"));
                return file.getAbsolutePath();
            } catch (Exception e) {
                Log.getStackTraceString(e);
                Log.i(LOG_TAG, ("===> Row not saved."));
            }
        }
        return null;
    }

    public boolean remove() {

        boolean removed = false;

        if (isExist()) {
            if (file.delete()) {
                Log.i(LOG_TAG, ("===> Row removed!"));
                removed = true;
            }
        }

        return removed;
    }
}
