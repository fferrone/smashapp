package it.smashapp.android.connection;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 */

public class PicassoFactory {


    private OkHttpClient client;
    public static PicassoFactory instance = new PicassoFactory();

    private PicassoFactory(){

        client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", RestUtil.getAuthToken())
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();

    }

    public Picasso getDownloader(Context context){

        Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();

        return picasso;
    }

}
