package it.smashapp.android.model;

import android.content.Context;

import it.smashapp.android.sql.DatabaseManager;

/**
 * Created by alessio on 12/18/16.
 */

public class OfflineModeDatabaseManager {

    long id;
    String name;
    String title;
    String source;
    String description;
    DatabaseManager dbm;

    public OfflineModeDatabaseManager(Context context){
        dbm = new DatabaseManager(context.getApplicationContext());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long saveOfflineCM(){
        id = dbm.insertCM(this);
        return id;
    }

    public void getIdByPath(String path){
        id = dbm.getIdByPathCM(path);
    }

    public long deleteOfflineCM(){
        return dbm.removeCM(id);
    }
}
