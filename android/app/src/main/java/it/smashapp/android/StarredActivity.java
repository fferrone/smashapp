package it.smashapp.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.layout.ImageListAdapter;
import it.smashapp.android.model.Image;
import it.smashapp.android.service.ImageService;
import it.smashapp.android.util.Constants;

public class StarredActivity extends AppCompatActivity {

    ArrayList<Image> imageList;
    private String username;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starred);

        Bundle extras = getIntent().getExtras();
        username = extras.getString(Constants.USERNAME);
        userId = extras.getString(Constants.USER_ID);
        TextView textUsername = (TextView) findViewById(R.id.starredUsername);
        textUsername.setText(username);
        ImageService.getStarredImages(userId, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                imageList = new ArrayList<>();
                try {
                    JSONArray images = response.getJSONObject("_embedded").getJSONArray("immagines");
                    for (int i = 0; i < images.length(); i++) {
                        Image img = new Image();
                        int id = images.getJSONObject(i).getInt("id");
                        String titolo = images.getJSONObject(i).getString("titolo");
                        String descrizione = images.getJSONObject(i).getString("descrizione");
                        String nome = images.getJSONObject(i).getString("nome");
                        String url = ImageService.getImageLink(id);
                        img.setId(id);
                        img.setUrl(url);
                        img.setDescription(descrizione);
                        img.setName(nome);
                        img.setTitle(titolo);
                        imageList.add(img);
                    }

                    ListView listview = (ListView) findViewById(R.id.starredListview);
                    ImageListAdapter listImageAdapter = new ImageListAdapter(imageList, StarredActivity.this);
                    listview.setAdapter(listImageAdapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            showImage(view, position);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                System.out.println("ERROR " + statusCode);
            }
        });
    }

    public void showImage(View view, int position) {
        Intent displayImage = new Intent(this, ImageDetailActivity.class);
        displayImage.putExtra("title", imageList.get(position).getTitle());
        displayImage.putExtra("url", imageList.get(position).getUrl());
        displayImage.putExtra("imageId", String.valueOf(imageList.get(position).getId()));
        displayImage.putExtra(Constants.USERNAME, username);
        displayImage.putExtra(Constants.USER_ID, userId);
        startActivity(displayImage);
    }
}
