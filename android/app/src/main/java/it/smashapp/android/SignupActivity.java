package it.smashapp.android;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class SignupActivity extends AppCompatActivity {

    TextView signinhere;

    Typeface fonts1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(it.smashapp.android.R.layout.signup);

        signinhere = (TextView) findViewById(it.smashapp.android.R.id.signinhere);

        signinhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SignupActivity.this, SigninActivity.class);
                startActivity(it);


            }
        });

        fonts1 = Typeface.createFromAsset(SignupActivity.this.getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView t1 = (TextView) findViewById(it.smashapp.android.R.id.signinhere);
        t1.setTypeface(fonts1);


    }
}
