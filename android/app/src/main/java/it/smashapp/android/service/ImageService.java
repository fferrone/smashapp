package it.smashapp.android.service;

import com.loopj.android.http.RequestParams;

import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.connection.RestUtil;

public class ImageService {

    public static void getImages(JsonResponseHandler handler) {
        RestUtil.get("immagines/search/findActiveImages", null, handler);
    }

    public static void getStarredImages(String userId, JsonResponseHandler handler) {
        RequestParams rp = new RequestParams();
        rp.put("user", userId);
        RestUtil.get("immagines/search/findStarredImages", rp, handler);
    }

    public static String getImageLink(int resourceId) {
        return RestUtil.getBaseUrl() + "resources/img/" + resourceId;
    }
}
