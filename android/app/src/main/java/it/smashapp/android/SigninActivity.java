package it.smashapp.android;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.layout.MyEditText;
import it.smashapp.android.service.LoginService;
import it.smashapp.android.util.Constants;

public class SigninActivity extends AppCompatActivity {

    private final static String LOG_TAG = "SigninActivity";

    TextView create;
    Typeface fonts1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);

        create = (TextView) findViewById(R.id.create);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SigninActivity.this, SignupActivity.class);
                startActivity(it);
            }
        });

        fonts1 = Typeface.createFromAsset(SigninActivity.this.getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView t4 = (TextView) findViewById(R.id.create);
        t4.setTypeface(fonts1);
        /* mock authentication */
        ((MyEditText) findViewById(R.id.email)).setText("user");
        ((MyEditText) findViewById(R.id.password)).setText("password");
    }

    public void goOn(View view) {

        String username = ((MyEditText) findViewById(R.id.email)).getText().toString();
        String password = ((MyEditText) findViewById(R.id.password)).getText().toString();

        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        LoginService.login(username, password, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                try {
                    String username = response.getString("username");
                    String userURI = response.getJSONObject("_links").getJSONObject("user").getString("href");
                    String[] splittedUserURI = userURI.split("/");
                    String userId = splittedUserURI[splittedUserURI.length - 1];
                    /* start new Intent */
                    Intent intent = new Intent(SigninActivity.this, HomeActivity.class);
                    intent.putExtra(Constants.USER_ID, userId);
                    intent.putExtra(Constants.USERNAME, username);
                    startActivity(intent);

                } catch (JSONException e) {
                    /* error */
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                System.out.println("ERROR " + statusCode);
                Intent intent = new Intent(SigninActivity.this, SigninActivity.class);
                startActivity(intent);
            }
        });
    }
}
