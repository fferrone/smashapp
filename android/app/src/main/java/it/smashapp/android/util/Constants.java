package it.smashapp.android.util;

/**
 *
 */

public class Constants {

    public final static String JWT_SECRET = "smashapp";
    public final static String USERNAME = "USERNAME";
    public final static String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String USER_ID = "USER_ID";
}
