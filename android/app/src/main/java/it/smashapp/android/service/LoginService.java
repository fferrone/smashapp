package it.smashapp.android.service;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.connection.RestUtil;
import it.smashapp.android.util.Constants;

public class LoginService {

    public static void login(String username, String password, final JsonResponseHandler handler) {

        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("password", password);

        RestUtil.post("login", params, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String token = response.getString("token");
                    Jws<Claims> claims = Jwts.parser()
                            .setSigningKey(Constants.JWT_SECRET)
                            .parseClaimsJws(token);
                    System.out.println("Logged in SUCCESSFULLY. JWT Token: " + token);
                    String username = claims.getBody().getSubject();
                    /*  persist authentication token    */
                    RestUtil.setAuthToken(token);
                    /*  RETRIEVE USER DETAILS   */
                    RequestParams params = new RequestParams();
                    params.put("username", username);
                    RestUtil.get("users/search/findByUsername", params, handler);

                } catch (JSONException e) {
                    /* error */
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                handler.onFailure(statusCode, headers, responseString);
            }
        });
    }

}