package it.smashapp.android;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.connection.PicassoFactory;
import it.smashapp.android.model.OfflineModeDatabaseManager;
import it.smashapp.android.service.PreferitiCMService;
import it.smashapp.android.util.Constants;

public class ImageDetailActivity extends AppCompatActivity {

    CheckBox likeCheckbox;
    CheckBox offModeCheckbox;
    String imageId;
    String username;
    String userId;
    String preferitoId;
    String title;
    ImageView img;
    OfflineModeFile omFile;
    OfflineModeDatabaseManager omDBManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);

        likeCheckbox = (CheckBox) findViewById(R.id.checkBoxLike);
        offModeCheckbox = (CheckBox) findViewById(R.id.checkBoxMO);

        likeCheckbox.setClickable(false);
        offModeCheckbox.setClickable(false);

        Bundle extras = getIntent().getExtras();
        title = extras.getString("title");
        String url = extras.getString("url");
        imageId = extras.getString("imageId");
        String descr = extras.getString("description");
        String name = extras.getString("name");

        username = extras.getString(Constants.USERNAME);
        userId = extras.getString(Constants.USER_ID);

        /* internal storage */
        omFile = new OfflineModeFile(getApplicationContext(), title);
        if(omFile.isExist()){
            /* retrieve from database, if exist */
            String path = omFile.getFilePath();
            System.out.println("===> File path: "+path);
            omDBManager = new OfflineModeDatabaseManager(getApplicationContext());
            omDBManager.getIdByPath(path);
        }
        else {
            /* prepare new content to store */
            omDBManager = new OfflineModeDatabaseManager(getApplicationContext());
        }

        /* set information for database */
        omDBManager.setTitle(title);
        omDBManager.setDescription(descr);
        omDBManager.setName(name);

        /* retrieve like if exist */
        getImagePreferito();

        TextView textView = (TextView) findViewById(R.id.CMtitle);
        textView.setText(title);
        img = (ImageView) findViewById(R.id.image);

        /* check if CM exist in file system.
        * if not exist, it is downloaded
        * otherwise it is loaded from android internal storage */
        if (!omFile.isExist()) {
            System.out.println("Downloading file from smashapp server.");
            Picasso picasso = PicassoFactory.instance.getDownloader(ImageDetailActivity.this);
            picasso.load(url)
                    .fit()
                    .centerInside()
                    /* Callback = keeps checkboxs unclicable, until the content isn't loaded */
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            likeCheckbox.setClickable(true);
                            offModeCheckbox.setClickable(true);
                        }

                        @Override
                        public void onError() { /* nothing to do */ }
                    });
        } else {
            Bitmap bitmap = omFile.getCM();
            img.setImageBitmap(bitmap);
            likeCheckbox.setClickable(true);
            offModeCheckbox.setClickable(true);
            offModeCheckbox.setChecked(true);
        }
    }

    public void likeCM(View view) {
        /* togli like */
        if (!likeCheckbox.isChecked() && !offModeCheckbox.isChecked()) {
            sendDisLike();
            System.out.println("==> 2");
            System.out.println("==> "+likeCheckbox.isChecked());
            System.out.println("==> "+offModeCheckbox.isChecked());
        }
        /* aggiungi like  */
        else if (likeCheckbox.isChecked() && !offModeCheckbox.isChecked()) {
            sendLike();
            System.out.println("===> 1");
            System.out.println("==> "+likeCheckbox.isChecked());
            System.out.println("==> "+offModeCheckbox.isChecked());
        }
        /* togli like e togli mo */
        else if (!likeCheckbox.isChecked() && offModeCheckbox.isChecked()){
            sendDisLike();
            omFile.remove();
            omDBManager.deleteOfflineCM();
            offModeCheckbox.setChecked(false);
        }
    }

    public void modOffline(View view) {
        /* aggiungi mo */
        if (likeCheckbox.isChecked() && offModeCheckbox.isChecked()) {
            Bitmap bm = ((BitmapDrawable) img.getDrawable()).getBitmap();
            String src = omFile.save(bm);
            omDBManager.setSource(src);
            omDBManager.saveOfflineCM();
            offModeCheckbox.setChecked(true);
            System.out.println("==> 4");
            System.out.println("==> "+likeCheckbox.isChecked());
            System.out.println("==> "+offModeCheckbox.isChecked());
        }
        /* togli mo */
        else if(likeCheckbox.isChecked() && !offModeCheckbox.isChecked()){
            /* rimozione del cm dal file system 1) dalla lista del db 2) */
            omFile.remove();
            System.out.println("Deleting row n. "+omDBManager.deleteOfflineCM());
            offModeCheckbox.setChecked(false);
            System.out.println("==> 6");
            System.out.println("==> "+likeCheckbox.isChecked());
            System.out.println("==> "+offModeCheckbox.isChecked());
        }
        /* aggiungi like e aggiungi mo */
        else if(!likeCheckbox.isChecked() && offModeCheckbox.isChecked()){
            sendLike();
            Bitmap bm = ((BitmapDrawable) img.getDrawable()).getBitmap();
            String src = omFile.save(bm);
            omDBManager.setSource(src);
            long id = omDBManager.saveOfflineCM();
            System.out.println("ID cm in database: "+id);
            offModeCheckbox.setChecked(true);
            System.out.println("==> 5");
            System.out.println("==> "+likeCheckbox.isChecked());
            System.out.println("==> "+offModeCheckbox.isChecked());
        }
    }

    private void sendDisLike() {
        PreferitiCMService.deletePreferitoCM(preferitoId, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                System.out.println(response);
                likeCheckbox.setChecked(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                /* on SUCCESS torna 204 ma viene considerato errore dalla libreria async-http */
                if (statusCode == 204) {
                    likeCheckbox.setChecked(false);
                } else {
                    System.out.println("ERROR " + statusCode);
                }
            }
        });
    }

    private void sendLike() {
        PreferitiCMService.createPreferitoCM(ImageDetailActivity.this, userId, imageId, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                likeCheckbox.setChecked(true);
                /* retrieve image preferito and update preferitoId */
                getImagePreferito();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                /* on SUCCESS torna 201 ma viene considerato errore dalla libreria async-http */
                if (statusCode == 201) {
                    likeCheckbox.setChecked(true);
                    /* retrieve image preferito and update preferitoId */
                    getImagePreferito();
                    System.out.println("Preferito inserito");
                } else {
                    System.out.println("ERROR " + statusCode);
                }
            }
        });
    }

    /*
     * retrieves the image preferito
     * if preferito does not exist returns 404
     */
    private void getImagePreferito() {
        PreferitiCMService.getPreferitoCM(userId, imageId, new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.length() > 0) {
                        String preferitoURI = response.getJSONObject("_links").getJSONObject("self").getString("href");
                        String[] splittedURI = preferitoURI.split("/");
                        preferitoId = splittedURI[splittedURI.length - 1];
                        likeCheckbox.setChecked(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String mesg) {
                /* 404 means no preferito */
            }
        });
    }

}
