package it.smashapp.android.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.util.ArrayList;

import it.smashapp.android.model.OfflineModeDatabaseManager;

/**
 * Created by alessio on 12/17/16.
 */

public class DatabaseManager {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "smashapp.db";
    private static final String TABLE_MOD_OFFLINE = "mod_offline";
    private static final String ID = "id";
    private static final String TITLE = "titolo";
    private static final String SOURCE = "path";
    private static final String NAME = "nome";
    private static final String DESCRIPTION = "descrizione";

    private SQLiteOpenHelper dbh;
    private Context context = null;

    /* Statements */
    private static final String SQL_CREATE_SMASHAPP_TABLES = "CREATE TABLE IF NOT EXISTS " + TABLE_MOD_OFFLINE + " ( " +
                                                        ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT DEFAULT 0, " +
                                                        NAME+ " TEXT, " +
                                                        TITLE + " TEXT NOT NULL, " +
                                                        SOURCE + " TEXT NOT NULL, " +
                                                        DESCRIPTION + " TEXT " +
                                                        ")";

    private static final String SQL_DELETE_MOD_OFFLINE = "DROP TABLE IF EXISTS " + TABLE_MOD_OFFLINE;

    public DatabaseManager(Context context) {
        this.context = context;
        dbh = new DBHelper(context);
        File database = context.getApplicationContext().getDatabasePath("databasename.db");
        System.out.println("===> Database path: "+database.toString());
    }

    public void close(){
        dbh.close();
    }

    public long insertCM(OfflineModeDatabaseManager offlineCM){
        SQLiteDatabase db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NAME, offlineCM.getName());
        values.put(TITLE, offlineCM.getTitle());
        values.put(SOURCE, offlineCM.getSource());
        values.put(DESCRIPTION, offlineCM.getDescription());

        long newRowId = db.insert(TABLE_MOD_OFFLINE, null, values);
        db.close();
        return newRowId;
    }

    public long getIdByPathCM(String path){
        SQLiteDatabase db = dbh.getReadableDatabase();
        long id = 0;

        String[] projection = {
                ID
        };

        String selection = SOURCE + " = ?";
        String[] selectionArgs = { path };

        Cursor cursor = db.query(
                TABLE_MOD_OFFLINE,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (cursor.moveToNext()){
            id = cursor.getLong(cursor.getColumnIndex(ID));
        }
        return id;
    }

    public ArrayList<OfflineModeDatabaseManager> readCM() {

        SQLiteDatabase db = dbh.getReadableDatabase();
        OfflineModeDatabaseManager cm = null;
        ArrayList<OfflineModeDatabaseManager> listCM = new ArrayList<OfflineModeDatabaseManager>();

        String[] projection = {
                ID,
                NAME,
                TITLE,
                DESCRIPTION,
                SOURCE
        };

        String sortOrder = ID + " DESC";

        Cursor cursor = db.query(
                TABLE_MOD_OFFLINE,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        if(cursor.moveToFirst()) {
            do {
                cm = new OfflineModeDatabaseManager(context);
                cm.setId(cursor.getLong(cursor.getColumnIndex(ID)));
                cm.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                cm.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                cm.setDescription(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
                cm.setSource(cursor.getString(cursor.getColumnIndex(SOURCE)));
                listCM.add(cm);
            }
            while (cursor.moveToNext());
        }

        db.close();
        return listCM;
    }

    public int removeCM(long id){

        SQLiteDatabase db = dbh.getWritableDatabase();

        String selection = ID + " LIKE ?";
        String[] selectionArgs = new String[]{ String.valueOf(id) };
        return db.delete(TABLE_MOD_OFFLINE, selection, selectionArgs);
    }

    /* inner class */
    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /* Override metodi onCreate, onUpgrade, onDowngrade */
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_SMASHAPP_TABLES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_MOD_OFFLINE);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }

    }

}
