package it.smashapp.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import it.smashapp.android.layout.OfflineModeListAdapter;
import it.smashapp.android.model.OfflineModeDatabaseManager;
import it.smashapp.android.sql.GeneralDB;

public class OfflineActivity extends AppCompatActivity {

    ArrayList<OfflineModeDatabaseManager> listOfflineCM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);

        /*
            bisogna salvare a db anche lo username e settarlo qui

            (TextView) findViewById(R.id.usernameOM).setText(username);

         */
        GeneralDB gen = new GeneralDB(getApplicationContext());
        listOfflineCM = gen.listOfflineCM();
        /* se ci sono CM offline da mostrare, invoca l'adapter  */
        if (!listOfflineCM.isEmpty()) {
            ListView listview = (ListView) findViewById(R.id.offlineListview);
            OfflineModeListAdapter offlineListAdapter = new OfflineModeListAdapter(listOfflineCM, OfflineActivity.this);
            listview.setAdapter(offlineListAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    /* Do something */
                    showImage(view, i);
                }
            });
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Nessun contenuto da mostrare");
            alertDialogBuilder.setMessage("Devi selezionare almeno \nun CM in MO");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(OfflineActivity.this, SigninActivity.class);
                    startActivity(intent);
                }
            });
            alertDialogBuilder.show();
        }

    }

    public void showImage(View view, int position) {
        Intent displayImage = new Intent(this, ImageDetailOfflineActivity.class);
        displayImage.putExtra("title", listOfflineCM.get(position).getTitle());
        displayImage.putExtra("url", listOfflineCM.get(position).getSource());
        displayImage.putExtra("imageId", String.valueOf(listOfflineCM.get(position).getId()));
        displayImage.putExtra("description", listOfflineCM.get(position).getDescription());
        displayImage.putExtra("name", listOfflineCM.get(position).getName());
        startActivity(displayImage);
    }
}
