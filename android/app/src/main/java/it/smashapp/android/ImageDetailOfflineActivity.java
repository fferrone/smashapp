package it.smashapp.android;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageDetailOfflineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail_offline);

        Bundle extras = getIntent().getExtras();
        String title = extras.getString("title");
        String url = extras.getString("url");
        String imageId = extras.getString("imageId");
        String descr = extras.getString("description");
        String name = extras.getString("name");

        TextView textView = (TextView) findViewById(R.id.headerTitleOM);
        textView.setText(title);
        OfflineModeFile omFile = new OfflineModeFile(getApplicationContext(), title);

        ImageView img = (ImageView) findViewById(R.id.imageOM);
        Bitmap bitmap = omFile.getCM();
        img.setImageBitmap(bitmap);
    }

}
