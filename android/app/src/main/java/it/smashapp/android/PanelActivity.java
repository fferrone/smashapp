package it.smashapp.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class PanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(it.smashapp.android.R.layout.activity_panel);


    }

    public void goToChangePasswd(View view){

        Intent changePwd = new Intent(this, ChangePasswordActivity.class);
        startActivity(changePwd);
    }
}
