package it.smashapp.android.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import it.smashapp.android.R;
import it.smashapp.android.connection.PicassoFactory;
import it.smashapp.android.model.Image;

/**
 *
 */

public class ImageListAdapter extends ArrayAdapter {

    private List<Image> imageList;
    private LayoutInflater layoutInflater;
    private Picasso picasso;

    public ImageListAdapter(List<Image> list, Context context) {
        super(context, R.layout.image_list_element, list);
        layoutInflater = LayoutInflater.from(context);
        imageList = list;
        picasso = PicassoFactory.instance.getDownloader(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            /* inflating is very expensive */
            convertView = layoutInflater.inflate(R.layout.image_list_element, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.desc = (TextView) convertView.findViewById(R.id.description);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Image image = imageList.get(position);
        viewHolder.title.setText(image.getTitle());
        viewHolder.desc.setText(image.getDescription());
        viewHolder.date.setText(image.getDate());

        /* locad image  */
        picasso.load(image.getUrl()).fit().centerInside().into(viewHolder.image);

        return convertView;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object getItem(int position) {
        return imageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        ImageView image;
        TextView title;
        TextView desc;
        TextView date;
    }
}
