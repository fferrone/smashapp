package it.smashapp.android.service;

import android.content.Context;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.connection.RestUtil;

public class PreferitiCMService {

    public static void getPreferitoCM(String userId, String imageId, JsonResponseHandler handler) {
        RequestParams rp = new RequestParams();
        rp.put("user", "api/users/" + userId);
        rp.put("contenutoMultimediale", "api/contenutoMultimediales/" + imageId);
        RestUtil.get("preferitiCMs/search/findByUserAndContenutoMultimediale", rp, handler);
    }

    public static void createPreferitoCM(Context context, String userId, String imageId, JsonResponseHandler handler) {
        JSONObject jsonParams = new JSONObject();
        StringEntity entity = null;
        try {
            jsonParams.put("user", "api/users/" + userId);
            jsonParams.put("contenutoMultimediale", "api/contenutoMultimediales/" + imageId);
            entity = new StringEntity(jsonParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RestUtil.postJSON(context, "preferitiCMs", entity, handler);
    }

    public static void deletePreferitoCM(String preferitoId, JsonResponseHandler handler){
        RestUtil.deleteJSON("preferitiCMs/" + preferitoId, handler);
    }

}