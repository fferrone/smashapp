package it.smashapp.android.connection;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.HttpEntity;

public class RestUtil {
    private static final String LOG_TAG = "RestUtil";
    private static final String BASE_URL = "http://smashapp-frontend.baloon.info/api/";
    //    private static final String BASE_URL = "http://192.168.2.104:8080/api/";
    private static String authToken;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, final AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Authorization", getAuthToken());
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, final AsyncHttpResponseHandler responseHandler) {
        Log.i(LOG_TAG, "Login url: " + getAbsoluteUrl(url));
        client.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void postJSON(Context context, String url, HttpEntity entity, final AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", getAuthToken());
        client.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
    }

    public static void deleteJSON(String url, final AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Authorization", getAuthToken());
        client.delete(getAbsoluteUrl(url), responseHandler);
    }

    public static String getAuthToken() {
        return "Barear " + authToken;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static void setAuthToken(String token) {
        authToken = token;
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
