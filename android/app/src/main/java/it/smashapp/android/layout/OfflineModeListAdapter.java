package it.smashapp.android.layout;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import it.smashapp.android.R;
import it.smashapp.android.model.OfflineModeDatabaseManager;

/**
 * Created by alessio on 12/19/16.
 */

public class OfflineModeListAdapter extends ArrayAdapter {

    private List<OfflineModeDatabaseManager> offlineCMListList;
    private LayoutInflater layoutInflater;

    public OfflineModeListAdapter(List<OfflineModeDatabaseManager> list, Context context) {
        super(context, R.layout.image_list_element, list);
        layoutInflater = LayoutInflater.from(context);
        offlineCMListList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OfflineModeListAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            /* inflating is very expensive */
            convertView = layoutInflater.inflate(R.layout.image_list_element, parent, false);
            viewHolder = new OfflineModeListAdapter.ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.desc = (TextView) convertView.findViewById(R.id.description);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (OfflineModeListAdapter.ViewHolder) convertView.getTag();
        }
        OfflineModeDatabaseManager offlineCM = offlineCMListList.get(position);
        viewHolder.title.setText(offlineCM.getTitle());
        viewHolder.desc.setText(offlineCM.getDescription());
        /* set ImageView from path prevoiusly stored */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(offlineCM.getSource(), bmOptions);
        viewHolder.image.setImageBitmap(bitmap);

        return convertView;
    }

    @Override
    public int getCount() {
        return offlineCMListList.size();
    }

    @Override
    public Object getItem(int position) {
        return offlineCMListList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView image;
        TextView title;
        TextView desc;
        TextView date;
    }
}
