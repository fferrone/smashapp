package it.smashapp.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import it.smashapp.android.connection.JsonResponseHandler;
import it.smashapp.android.layout.ImageListAdapter;
import it.smashapp.android.model.Image;
import it.smashapp.android.service.ImageService;
import it.smashapp.android.util.Constants;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ArrayList<Image> imageList;
    private String username;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Bundle extras = getIntent().getExtras();
        username = extras.getString(Constants.USERNAME);
        userId = extras.getString(Constants.USER_ID);
        TextView textUsername = (TextView) findViewById(R.id.homeUsername);
        textUsername.setText(username);

        ImageService.getImages(new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                imageList = new ArrayList<>();
                try {
                    JSONArray images = response.getJSONObject("_embedded").getJSONArray("immagines");
                    for (int i = 0; i < images.length(); i++) {
                        Image img = new Image();
                        int id = images.getJSONObject(i).getInt("id");
                        String titolo = images.getJSONObject(i).getString("titolo");
                        String descrizione = images.getJSONObject(i).getString("descrizione");
                        String nome = images.getJSONObject(i).getString("nome");
                        String url = ImageService.getImageLink(id);
                        img.setId(id);
                        img.setUrl(url);
                        img.setDescription(descrizione);
                        img.setName(nome);
                        img.setTitle(titolo);
                        imageList.add(img);
                    }
                    ListView listview = (ListView) findViewById(R.id.listView);
                    ImageListAdapter listImageAdapter = new ImageListAdapter(imageList, HomeActivity.this);
                    listview.setAdapter(listImageAdapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            showImage(view, position);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString) {
                System.out.println("ERROR " + statusCode);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(it.smashapp.android.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(it.smashapp.android.R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == it.smashapp.android.R.id.nav_panel) {
            Intent intent = new Intent(this, PanelActivity.class);
            startActivity(intent);
        } else if (id == it.smashapp.android.R.id.nav_logout) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == it.smashapp.android.R.id.nav_starred) {
            Intent intent = new Intent(this, StarredActivity.class);
            intent.putExtra(Constants.USERNAME, username);
            intent.putExtra(Constants.USER_ID, userId);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(it.smashapp.android.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showImage(View view, int position) {
        Intent displayImage = new Intent(this, ImageDetailActivity.class);
        displayImage.putExtra("title", imageList.get(position).getTitle());
        displayImage.putExtra("url", imageList.get(position).getUrl());
        displayImage.putExtra("imageId", String.valueOf(imageList.get(position).getId()));
        displayImage.putExtra("description", imageList.get(position).getDescription());
        displayImage.putExtra("name", imageList.get(position).getName());
        displayImage.putExtra(Constants.USERNAME, username);
        displayImage.putExtra(Constants.USER_ID, userId);
        startActivity(displayImage);
    }
}
