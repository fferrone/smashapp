package it.smashapp.android.connection;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public abstract class JsonResponseHandler extends JsonHttpResponseHandler {

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        this.onFailure(statusCode, headers, responseString);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject responseString) {
        if(responseString == null){
            responseString = new JSONObject();
        }
        this.onFailure(statusCode, headers, responseString.toString());
    }

    public abstract void onFailure(int statusCode, Header[] headers, String mesg);
}
