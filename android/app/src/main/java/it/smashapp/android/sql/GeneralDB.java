package it.smashapp.android.sql;

import android.content.Context;

import java.util.ArrayList;

import it.smashapp.android.model.OfflineModeDatabaseManager;

/**
 * Created by alessio on 12/19/16.
 */

public class GeneralDB {

    DatabaseManager dbm;

    public GeneralDB(Context context){
        dbm = new DatabaseManager(context.getApplicationContext());
    }

    /* metodi generici per la gestione di smashapp */

    public ArrayList<OfflineModeDatabaseManager> listOfflineCM(){
        return dbm.readCM();
    }
}
