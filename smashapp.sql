-- MySQL dump 10.16  Distrib 10.1.18-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: smashapp
-- ------------------------------------------------------
-- Server version	10.1.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `canale`
--

DROP TABLE IF EXISTS `canale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `titolo` varchar(200) NOT NULL,
  `descrizione` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `canale_nome_uindex` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canale`
--

LOCK TABLES `canale` WRITE;
/*!40000 ALTER TABLE `canale` DISABLE KEYS */;
INSERT INTO `canale` VALUES (1,'CANALE 1','TITOLO CANALE 1','Descrizione Canale 1'),(2,'fdsfs','dfssd','dsa'),(4,'frfwfwef','dqdw','dqwswq'),(5,'uno','uno','unouno'),(6,'due','due','duedue');
/*!40000 ALTER TABLE `canale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canale_redattore`
--

DROP TABLE IF EXISTS `canale_redattore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canale_redattore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `canale` int(11) NOT NULL,
  `redattore` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `canale_redattore_canale_redattore_uindex` (`canale`,`redattore`),
  KEY `canale_redattore_redattore` (`redattore`),
  CONSTRAINT `canale_redattore_canale` FOREIGN KEY (`canale`) REFERENCES `canale` (`id`),
  CONSTRAINT `canale_redattore_redattore` FOREIGN KEY (`redattore`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canale_redattore`
--

LOCK TABLES `canale_redattore` WRITE;
/*!40000 ALTER TABLE `canale_redattore` DISABLE KEYS */;
INSERT INTO `canale_redattore` VALUES (3,1,1),(4,5,1),(5,6,1);
/*!40000 ALTER TABLE `canale_redattore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_cm`
--

DROP TABLE IF EXISTS `categoria_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descrizione` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categoria_cm_nome_uindex` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_cm`
--

LOCK TABLES `categoria_cm` WRITE;
/*!40000 ALTER TABLE `categoria_cm` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_categoria_cm`
--

DROP TABLE IF EXISTS `cm_categoria_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_categoria_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenuto_multimediale` int(11) NOT NULL,
  `categoria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cm_categoria_cm_unique_categoria_per_cm` (`categoria`,`contenuto_multimediale`),
  KEY `cm_categoria_cm_contenuto_multimediale_categoria_index` (`contenuto_multimediale`,`categoria`),
  CONSTRAINT `cm_categoria_cm_categoria` FOREIGN KEY (`categoria`) REFERENCES `categoria_cm` (`id`),
  CONSTRAINT `cm_categoria_cm_cm` FOREIGN KEY (`contenuto_multimediale`) REFERENCES `contenuto_multimediale` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_categoria_cm`
--

LOCK TABLES `cm_categoria_cm` WRITE;
/*!40000 ALTER TABLE `cm_categoria_cm` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_categoria_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenuto_multimediale`
--

DROP TABLE IF EXISTS `contenuto_multimediale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenuto_multimediale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `titolo` varchar(200) NOT NULL,
  `descrizione` text,
  `stato` int(11) NOT NULL,
  `canale` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contenuto_multimediale_nome_uindex` (`nome`),
  KEY `contenuto_multimediale_stato` (`stato`),
  KEY `contenuto_multimediale_canale` (`canale`),
  CONSTRAINT `contenuto_multimediale_canale` FOREIGN KEY (`canale`) REFERENCES `canale` (`id`),
  CONSTRAINT `contenuto_multimediale_stato` FOREIGN KEY (`stato`) REFERENCES `stato_cm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenuto_multimediale`
--

LOCK TABLES `contenuto_multimediale` WRITE;
/*!40000 ALTER TABLE `contenuto_multimediale` DISABLE KEYS */;
/*!40000 ALTER TABLE `contenuto_multimediale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_cm`
--

DROP TABLE IF EXISTS `evento_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_evento` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `contenuto_multimediale` int(11) DEFAULT NULL,
  `data` datetime NOT NULL,
  `nota` text,
  `event_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `evento_cm_tipo_evento` (`tipo_evento`),
  KEY `evento_cm_user` (`user`),
  KEY `evento_cm_cm` (`contenuto_multimediale`),
  CONSTRAINT `evento_cm_cm` FOREIGN KEY (`contenuto_multimediale`) REFERENCES `contenuto_multimediale` (`id`),
  CONSTRAINT `evento_cm_tipo_evento` FOREIGN KEY (`tipo_evento`) REFERENCES `tipo_evento_cm` (`id`),
  CONSTRAINT `evento_cm_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_cm`
--

LOCK TABLES `evento_cm` WRITE;
/*!40000 ALTER TABLE `evento_cm` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `immagine`
--

DROP TABLE IF EXISTS `immagine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immagine` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `immagine_cm` FOREIGN KEY (`id`) REFERENCES `contenuto_multimediale` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `immagine`
--

LOCK TABLES `immagine` WRITE;
/*!40000 ALTER TABLE `immagine` DISABLE KEYS */;
/*!40000 ALTER TABLE `immagine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferiti_user_canale`
--

DROP TABLE IF EXISTS `preferiti_user_canale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferiti_user_canale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `canale` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `preferiti_user_canale_user_canale_uindex` (`user`,`canale`),
  KEY `preferiti_user_canale_canale_id_fk` (`canale`),
  CONSTRAINT `preferiti_user_canale_canale_id_fk` FOREIGN KEY (`canale`) REFERENCES `canale` (`id`),
  CONSTRAINT `preferiti_user_canale_user_id_fk` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferiti_user_canale`
--

LOCK TABLES `preferiti_user_canale` WRITE;
/*!40000 ALTER TABLE `preferiti_user_canale` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferiti_user_canale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferiti_user_categoria`
--

DROP TABLE IF EXISTS `preferiti_user_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferiti_user_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `preferiti_user_categoria_user_categoria_uindex` (`user`,`categoria`),
  KEY `preferiti_user_categoria_categoria_cm_id_fk` (`categoria`),
  CONSTRAINT `preferiti_user_categoria_categoria_cm_id_fk` FOREIGN KEY (`categoria`) REFERENCES `categoria_cm` (`id`),
  CONSTRAINT `preferiti_user_categoria_user_id_fk` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferiti_user_categoria`
--

LOCK TABLES `preferiti_user_categoria` WRITE;
/*!40000 ALTER TABLE `preferiti_user_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferiti_user_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferiti_user_cm`
--

DROP TABLE IF EXISTS `preferiti_user_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferiti_user_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `contenuto_multimediale` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `preferiti_user_user_contenuto_multimediale_uindex` (`user`,`contenuto_multimediale`),
  KEY `preferiti_user_cm` (`contenuto_multimediale`),
  CONSTRAINT `preferiti_user_cm` FOREIGN KEY (`contenuto_multimediale`) REFERENCES `contenuto_multimediale` (`id`),
  CONSTRAINT `preferiti_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferiti_user_cm`
--

LOCK TABLES `preferiti_user_cm` WRITE;
/*!40000 ALTER TABLE `preferiti_user_cm` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferiti_user_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ruolo_user`
--

DROP TABLE IF EXISTS `ruolo_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ruolo_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ruolo_user`
--

LOCK TABLES `ruolo_user` WRITE;
/*!40000 ALTER TABLE `ruolo_user` DISABLE KEYS */;
INSERT INTO `ruolo_user` VALUES (1,'ADMIN'),(2,'APP_USER'),(3,'REDATTORE'),(4,'APPROVATORE'),(5,'GUEST');
/*!40000 ALTER TABLE `ruolo_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stato_cm`
--

DROP TABLE IF EXISTS `stato_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stato_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stato_cm`
--

LOCK TABLES `stato_cm` WRITE;
/*!40000 ALTER TABLE `stato_cm` DISABLE KEYS */;
INSERT INTO `stato_cm` VALUES (1,'IN PROPOSAL'),(2,'INACTIVE'),(3,'DELETED'),(4,'LOCKED'),(5,'APPROVED'),(6,'REJECTED');
/*!40000 ALTER TABLE `stato_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stato_user`
--

DROP TABLE IF EXISTS `stato_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stato_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stato_user`
--

LOCK TABLES `stato_user` WRITE;
/*!40000 ALTER TABLE `stato_user` DISABLE KEYS */;
INSERT INTO `stato_user` VALUES (1,'ACTIVE'),(2,'INACTIVE'),(3,'DELETED'),(4,'LOCKED');
/*!40000 ALTER TABLE `stato_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_evento_cm`
--

DROP TABLE IF EXISTS `tipo_evento_cm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_evento_cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_evento_cm`
--

LOCK TABLES `tipo_evento_cm` WRITE;
/*!40000 ALTER TABLE `tipo_evento_cm` DISABLE KEYS */;
INSERT INTO `tipo_evento_cm` VALUES (1,'INSERT'),(2,'DELETE'),(3,'APPROVE'),(4,'REJECT'),(5,'SIGNAL');
/*!40000 ALTER TABLE `tipo_evento_cm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` char(60) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `stato` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_stato` (`stato`),
  CONSTRAINT `user_stato` FOREIGN KEY (`stato`) REFERENCES `stato_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'red1','$2a$11$SvWcE/abPoAWCq3/rhdfGeqGeMAB.DGQX4xILC/X/FHLBQkVcbvOq','redattore','uno','red@mail.it',1,1),(2,'admin','$2a$11$SvWcE/abPoAWCq3/rhdfGeqGeMAB.DGQX4xILC/X/FHLBQkVcbvOq','nome admin','cognome admin','admin@mail.it',1,1),(3,'approvatore1','$2a$11$y8Ziu4oTOLwqWFX/87MQruROg5l5sPIvH5xLFdI1gavDlT18UpkEO',NULL,NULL,NULL,1,1),(4,'user','$2a$11$lHUiuYNZMJ6h7pdD73PAKebEJ0cnIpZFiHIGqfAA3RLMmQNWprBx6',NULL,NULL,NULL,1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_ruolo_user`
--

DROP TABLE IF EXISTS `user_ruolo_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_ruolo_user` (
  `user` int(11) NOT NULL,
  `ruolo_user` int(11) NOT NULL,
  UNIQUE KEY `user_ruolo_user_user_ruolo_user_uindex` (`user`,`ruolo_user`),
  KEY `user_ruolo_user__ruolo_user` (`ruolo_user`),
  CONSTRAINT `user_ruolo_user__ruolo_user` FOREIGN KEY (`ruolo_user`) REFERENCES `ruolo_user` (`id`),
  CONSTRAINT `user_ruolo_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_ruolo_user`
--

LOCK TABLES `user_ruolo_user` WRITE;
/*!40000 ALTER TABLE `user_ruolo_user` DISABLE KEYS */;
INSERT INTO `user_ruolo_user` VALUES (1,3),(2,1),(3,4),(4,2);
/*!40000 ALTER TABLE `user_ruolo_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `url` varchar(300) NOT NULL,
  `durata` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `video_cm` FOREIGN KEY (`id`) REFERENCES `contenuto_multimediale` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-22 22:42:29
